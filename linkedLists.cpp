#include <iostream>
#include <cstddef>
#include <cstdlib>
#include "linkedLists.h"
using namespace std;

/*
   Linked lists
1. Node constructors
2. Node destructor
3. AddElement()
4. PrintList()
5. DeleteList()
6. FindElement()
7. DeleteNode()
8. FindMinList()
9. SelecctionSortList()
10. InsertElementInOrder()
11. InsertionSortList()
12. GetMiddleElement()
13. MergeLists()
14. MergeSortList()
15. GetLastElement()
16. PartitionList()
17. QuickSortList()
*/

// 1.Node constructors
Node::Node(){
	next = NULL;
}

Node::Node(int pKey){
	next = NULL;
	key = pKey;
	value = -1;
}

Node::Node(int pKey, int pValue){
	next = NULL;
	key = pKey;
	value = pValue;
}

Node::Node(int pKey, int pValue, Node* pNode){
	next = pNode;
	key = pKey;
	value = pValue;
}

// 2.Node destructor
Node::~Node(){}

// 3.AddElement()
// a) to a list, key passed as argument
Node* AddElement(Node* list, int pKey){
	Node* node = new Node(pKey);
	return AddElement(list, node);
}

// b) to a list, key and value passed as arguments
Node* AddElement(Node* list, int pKey, int pValue){
	Node* node = new Node(pKey, pValue);
	return AddElement(list, node);
}

// c) to a list, node passed as argument
Node* AddElement(Node* list, Node* node){
	if(list == NULL and node == NULL){
		cout << "Cannot add empty element to empty list" << endl;
		return NULL;
	}
	else if(node == NULL){
		cout << "Cannot add empty element to list" << endl;
		return list;
	}
	else if(list == NULL){
		return node;
	}
	else{
		Node* tmp = list;
		while(tmp->next != NULL){
			tmp = tmp->next;
		}
		tmp->next = node;
		node->next = NULL;
		return list;	
	}
}

// d) to a list, do not return pointer to first element of a list
void AddElementRef(Node* &list, Node* node){
	if(list == NULL and node == NULL){
		cout << "Cannot add empty element to empty list" << endl;
		return;
	}
	else if(node == NULL){
		cout << "Cannot add empty element to list" << endl;
		return;
	}
	else if(list == NULL){
		list = node;
	}
	else{
		Node* tmp = list;
		while(tmp->next != NULL){
			tmp = tmp->next;
		}
		tmp->next = node;
		node->next = NULL;
		return;
	}
}

// 4.PrintList()
void PrintList(Node* list){
	Node* tmp = list;
	while(tmp != NULL){
		cout << "Key:		" << tmp->key << " Value: 	" << tmp->value << endl;
		tmp = tmp->next;
	}
}

// 5.DeleteList()
void DeleteList(Node* list){
	if(list->next != NULL)
		DeleteList(list->next);
	delete list;
}

// 6.FindElement()
Node* FindElement(Node* list, int pKey){
	if(list == NULL){
		cout << "Cannot find element in empty list" << endl;
		return NULL;
	}
	else{
		Node* tmp = list;
		while(tmp->key != pKey and tmp->next != NULL)
			tmp = tmp->next;
		if(tmp->key != pKey){
			//cout << "Element not found" << endl;
			return NULL;
		}
		else{
			//cout << "Element found. Key: " << tmp->key << "  Value: " << tmp->value << endl;
			return tmp;
		}
		
	}
}

// 7.DeleteNode()
// a) by value
void DeleteNode(Node* list, int pKey){
	Node* tmp = FindElement(list, pKey);
	DeleteNode(list, tmp);
}


// b) by node address 
void DeleteNode(Node* &list, Node* node){
	if(list == NULL){
		cout << "Cannot delete element from empty list" << endl;
		return;
	}
	else if(node == NULL){
		cout<< "Cannot delete non existing element" << endl;
	}
	else{
		Node* sentinel = new Node();
		sentinel->next = list;
		Node* tmp = sentinel;
		while(tmp->next!=NULL){
			if(tmp->next == node){
				if(tmp == sentinel){									// if node is first element of list
					list = list->next;
					delete tmp->next;
				}
				else{													// if node is somewhere in the middle of list
					Node* tmp2 = tmp->next;
					tmp->next = tmp->next->next;
					delete tmp2;
				}
				break;
			}
			tmp = tmp->next;
		}
		delete sentinel;
	}
}

// 8.FindMinList()
Node* FindMinList(Node* list){
	if(list == NULL){
		cout << "Cannot find min element in empty list" << endl;
		return NULL;
	}
	else{
		int tmpMin = list->key;
		Node* tmp, *tmpNode;
		tmp = tmpNode = list;
		while(tmp != NULL){
			if(tmp->key < tmpMin){									
				tmpMin = tmp->key; 										// remember the key of min key element
				tmpNode = tmp;											// remember the address of min key element
			}
			tmp = tmp->next;
		}
		return tmpNode;
	}
}

// 9.SelectionSortList()
Node* SelectionSortList(Node* list){
	if(list == NULL){
		cout << "Cannot sort an empty list" << endl;
		return NULL;
	}
	else{
		Node* sorted = new Node();
		Node* tmp,* tmpList;
		tmp = tmpList = sorted;
		while(list != NULL){
			tmp = FindMinList(list); 									// find node with min key in list
			tmpList->next = new Node(tmp->key, tmp->value); 	 		// create new node exactly like that min node in list
			tmpList = tmpList->next; 						 			// and append it to sorted list
			DeleteNode(list, tmp); 										// delete that min node in first list
		}
		tmpList = sorted->next;
		delete sorted; 													// free memory after sentinel
		return tmpList; 
	}
}

// 10.InsertElementInOrder()
// a) by key and value
void InsertElementInOrder(Node* &list, int pKey, int pValue){
	if(list == NULL){
		list = new Node(pKey, pValue);
		return;
	}
	else{
		Node* node = new Node(pKey, pValue);
		InsertElementInOrder(list, node);
	}
}

// b) by node
void InsertElementInOrder(Node* &list, Node* node){
	if(list == NULL){
		list = node;
		node->next = NULL;
		return;
	}
	else{
		Node* sentinel = new Node(); 										
		sentinel->next = list;											// set up sentinel
		Node* tmpList = sentinel;
		while(tmpList->next != NULL and tmpList->next->key < node->key) // while next element is larger and is not null
			tmpList = tmpList->next;
		if(tmpList->next == NULL){ 										// if inserted element should be the last element
			tmpList->next = node;
			tmpList->next->next = NULL;
		}
		else{
			if(tmpList->next == sentinel->next){ 						// if inserted element should be first element
				list = node;
				node->next = tmpList->next;
			}
			else{ 														// otherwise
				node->next = tmpList->next;
				tmpList->next = node;
			}
		}
		delete sentinel;                                				// free memory after sentinel
	}
}


// 11.InsertionSortList()
Node* InsertionSortList(Node* list){
	if(list == NULL){
		cout << "Cannot sort an empty list" << endl;
		return NULL;
	}
	else{
		Node* sorted = NULL;
		Node* tmpList = list;
		while(tmpList != NULL){
			Node* tmp = tmpList->next;									// remember the address of next node in list
			InsertElementInOrder(sorted, tmpList); 						// insert current node into sorted list
			tmpList = tmp; 												// restore current list pointer
		}
		return sorted;
	}
}

// 12.GetMiddleElement()
Node* GetMiddleElement(Node* list){
	if(list == NULL){
		cout << "Cannot get middle element of an empty list" << endl;
		return NULL;
	}
	else{
		Node* fast, * slow;
		fast = slow = list;
		while(fast != NULL){
			if(fast->next != NULL and fast->next->next != NULL)
				fast = fast->next->next;
			else break;
			slow = slow->next;
		}
		return slow;
	}
}

// 13.MergeLists()
Node* MergeLists(Node* list1, Node* list2){
	Node* result = new Node();
	Node* tmpResult = result;
	result->next = NULL;
	while(list1 != NULL and list2 != NULL){ 							// if there are unused elements in list1 and list 2
		if(list1->key < list2->key){
			tmpResult->next = list1;
			list1 = list1->next;
		}
		else{
			tmpResult->next = list2;
			list2 = list2->next;
		}
		tmpResult = tmpResult->next;
		tmpResult->next = NULL;
	}
	if(list1 != NULL)													// if either list is empty, just append to result list non-empty list
		tmpResult->next = list1;
	if(list2 != NULL)
		tmpResult->next = list2;

	tmpResult = result->next;
	delete result;
	return tmpResult;
}

// 14.MergeSortList()
Node* MergeSortList(Node* list){
	if(list == NULL or list->next == NULL)
		return list;
	else{
		Node* middle = GetMiddleElement(list);
		Node* list1,* list2;
		list1 = list;
		list2 = middle->next; 											// separate halves of list
		middle->next = NULL;
		return MergeLists(MergeSortList(list1), MergeSortList(list2));
	}
}

// 15.GetLastElement()
Node* GetLastElement(Node* list){
	if(list == NULL)
		return NULL;
	else{
		while(list->next != NULL)
			list = list->next;

		return list;
	}
}

// 16.PartitionList()
Node* PartitionList(Node* &list){ 										// returned node* will be pointing to smaller list, list passed by reference will contain biggerOrEqual elements
	if(list == NULL or list->next == NULL)
		return list;
	else{
		Node* tmp1,* tmp2;												// tmp nodes for remembering sentinels' addresses
		Node* smaller = new Node();
		Node* bigger = new Node();
		tmp1 = smaller;
		tmp2 = bigger;
		smaller->next = bigger->next = NULL; 							// set up ends of each sublist
		Node* pivot = list; 											// select pivot (first element)
		list = list->next; 
		pivot->next = NULL;
		while(list != NULL){
			if(list->key < pivot->key){ 								// if current node->key is smaller than pivot->key, append it to 'smaller' list
				smaller->next = list;
				list = list->next;
				smaller = smaller->next;
				smaller->next = NULL;
			}
			else{ 														// otherwise append it to 'bigger' list
				bigger->next = list;
				list = list->next;
				bigger = bigger->next;
				bigger->next = NULL;
			}
		}
		list = pivot; 													// append pivot to 'bigger' list
		list->next = tmp2->next;
		delete tmp2; 													// free memory after sentinel node #2
		smaller = tmp1->next;
		delete tmp1; 													// and #1
		return smaller; 												// return pointer to first node of 'smaller' list
	}
}

// 17.QuickSortList()
Node* QuickSortList(Node* list){
	if(list == NULL or list->next == NULL)
		return list;
	else{
		Node* smaller = PartitionList(list); 							
		Node* pivot = list;
		list = list->next; 												// now list == 'bigger' from PartitionList
		pivot->next = NULL; 
		Node* result1, * result2;
		result1 = QuickSortList(smaller); 								// recursively sort 'smaller' and 'bigger' lists
		result2 = QuickSortList(list);
		Node* tmp = GetLastElement(result1);
		if(tmp != NULL){ 												// if 'smaller' list is not empty
			tmp->next = pivot; 						 					// connect 'smaller', pivot and 'bigger'
			pivot->next = result2;
			return result1;												// return address of head of 'smaller'
		}
		else{ 															// otherwise, connect pivot and 'bigger'
			pivot->next = result2; 									
			return pivot; 												// return address of pivot
		}
	}
}