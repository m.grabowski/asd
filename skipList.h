// skipList.h
#ifndef SKIPLIST_H
#define SKIPLIST_H

struct SLNode;
struct SkipList;

struct SLNode{
	SLNode** next;
	int key, value;

	SLNode();
	SLNode(int pKey);
	SLNode(int pKey, int pValue);
	~SLNode();
};

struct SkipList{
	SLNode* head;
	int height;

	SkipList();
	SkipList(int pHeight);
	~SkipList();
};

// Delete and Print SkipList
void DeleteSkipList(SkipList* list);
void DeleteSkipList(SLNode* list);
void PrintSkipList(SkipList* list);

// Functions for adding elements
int GetHeight(int maxHeight);
void AddSLNode(SkipList* list, int pKey);
void AddSLNode(SkipList* list, int pKey, int pValue);

// Find node by key
SLNode* FindSLNode(SkipList* list, int pKey);

// Delete node by key
void DeleteSLNode(SkipList* list, int pKey);

#endif