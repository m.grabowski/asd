#include <iostream>
#include "findUnion.h"

/*
   Find&Union
1. Data constructors and destructor
2. Subset constructor and destructor
3. MakeSet()
4. Find()
5. Union()
*/

// 1.Data constructors and destructor
Data::Data(){}

Data::Data(int pKey){
	key = pKey;
	value = -1;
}

Data::Data(int pKey, int pValue){
	key = pKey;
	value = pValue;
}

Data::~Data(){}

// 2.Subset constructor and destructor
Subset::Subset(){
	rank = 0;
	parent = NULL;
}

Subset::Subset(Data pData){
	d = pData;
	rank = 0;
	parent = NULL;
}

Subset::~Subset(){}

// 3.MakeSet()
void MakeSet(Subset* set){
	set->rank = 0;
	set->parent = NULL;
}

// 4.Find()
Subset* Find(Subset* set){
	if(set->parent == NULL)
		return set;
	set->parent = Find(set->parent);
	return set->parent;
}

// 5.Union()
void Union(Subset* set1, Subset* set2){
	Subset* root1 = Find(set1);
	Subset* root2 = Find(set2);
	if(root1->rank > root2->rank)
		root2->parent = root1;
	else if(root1->rank < root2->rank)
		root1->parent = root2;
	else{
		root2->parent = root1;
		root1->rank++;
	}
}