// graph.h
#ifndef GRAPH_H
#define GRAPH_H

struct Graph;
struct Vertex;
struct Edge;

struct Graph{
	bool digraph;
	int verticesNumber, edgesNumber;
	int** matrix;
	Vertex** list;

	Graph();
	Graph(int vertices);
	Graph(int vertices, int edges);
	Graph(const Graph& g);
	~Graph();
};

struct Vertex{
	int number;
	int weight;
	Vertex* next;

	Vertex();
	Vertex(int index);
	Vertex(int index, int pWeight);
	~Vertex();
};

struct Edge{
	int vertex1, vertex2;
	int weight;
	Edge();
	Edge(int index1, int index2);
	Edge(int index1, int index2, int pWeight);
	~Edge();
};

// Creating, copying and printing graph
void CreateGraph(Graph& G, int vertices, int edges, bool version);
void PrintGraph(Graph& G);
void CopyGraph(Graph& G, Graph& newGraph);

// Convert graph to another representation
void GraphMatrixToList(Graph& G);
void GraphListToMatrix(Graph& G);

// DepthFirstSearch
void DFS(Graph& G);
void DFS(Graph& G, int start);
void DFSVisit(Graph& G, int vertex, int& time, int* enterTime, int* finishTime, int* parent, bool* visited);

// BreadthFirstSearch
void BFS(Graph& G, int start);

// Transpose graph
void Transposition(Graph &G);

// Build graph from digraph
Graph BuildGraphFromDigraph(Graph& G);

// Graph degree
int GraphDegree(Graph& G);

// Check connectivity, find connected components and strongly connected components
bool Connectivity(Graph& G);
void ConnectedComponentsDFS(Graph& G, int vertex, int* component, bool* visited);
void ConnectedComponents(Graph& G);
void StronglyConnectedComponents(Graph& G);
void StronglyConnectedComponentsDFS(Graph& G, int vertex, int& time, Vertex* &finishTime, bool* visited);

// Find bridges
void FindBridges(Graph& G);
int FindBridgesDFS(Graph& G, int vertex, int parent, int* vertexNumber, bool* visited, int& vertexCounter);

// Check if graph has an eulerian cycle or path
void EulerianCyclePath(Graph& G);
void EulerianCyclePathDFS(Graph& G, int vertex, bool* visited, int& oddCounter);

// Find minimum spanning tree
int MSTPrim(Graph& G);
int MSTPrim(Graph& G, int startVertex, int* parent, int* key, bool* visited);
int MSTKruskal(Graph& G);
void SortEdges(Edge* array, int size);
void EdgeQS(Edge* array, int start, int end);
int EdgePartition(Edge* array, int start, int end);

// Find shortest paths from one vertex to all other vertices
void Dijkstra(Graph& G, int start);
bool BellmanFord(Graph& G, int start);

// Find shortest paths between all pairs of graphs
void FloydWarshall(Graph& G);
void PrintFloydWarshallPath(int start, int end, int** parent);

#endif
