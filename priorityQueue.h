// priorityQueue.h
#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

struct T;
struct PriorityQueue;

struct T{
	int key, value;

	T();
	T(int pKey);
	T(int pKey, int pValue);
	~T();
};

struct PriorityQueue{
	T* queueArray;
	int arraySize;
	int realSize;
	int max;

	PriorityQueue();
	PriorityQueue(int size);
	PriorityQueue(const PriorityQueue& queue);
	~PriorityQueue();
};

// Swap for T type
void Swap(T& x, T& y);

// 
int Left(int index);
int Right(int index);
int Parent(int index);

// Building and restoring heap property
void HeapifyMin(PriorityQueue& Q, int index);
void BuildHeapMin(PriorityQueue& Q);

// Procedures for adding elements to queue
void ResizePriorityQueue(PriorityQueue& Q);
void AddElement(PriorityQueue& Q, T element);

// Get first element of queue
T FirstElement(PriorityQueue Q);
T RemoveMin(PriorityQueue& Q);

// Check if queue is empty
bool QueueEmpty(PriorityQueue Q);

#endif