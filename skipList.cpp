#include <iostream>
#include <cstdlib>
#include <ctime>
#include "skipList.h"
#include "limits.h"
using namespace std;

/*
   SkipList
1. SLNode constructors
2. SLNode destructor
3. SkipList constructors
4. SkipList destructor
5. DeleteSkipList()
6. PrintSkipList()
7. GetHeight() - get height for next SLNode
8. AddNode() 
9. FindSLNode()
10. DeleteSLNode()
*/

// 1.SLNode constructors
SLNode::SLNode(){
	next = NULL;
}

SLNode::SLNode(int pKey){
	next = NULL;
	key = pKey;
}

SLNode::SLNode(int pKey, int pValue){
	next = NULL;
	key = pKey;
	value = pValue;
}

// 2.SLNode destructor
SLNode::~SLNode(){}

// 3.SkipList constructors
SkipList::SkipList(){
	head = NULL;
}

SkipList::SkipList(int pHeight){
	if(pHeight > 0){
		height = pHeight;
		head = new SLNode();
		head->key = head->value = INT_MIN;
		head->next = new SLNode*[height];
		for(int i = 0; i < height; i++)
			head->next[i] = NULL;
	}
}

// 4.SkipList destructor
SkipList::~SkipList(){}

// 5.Delete Skiplist
// a) whole list
void DeleteSkipList(SkipList* list){
	DeleteSkipList(list->head);
	delete list;
}

// b) part of list starting from specific node
void DeleteSkipList(SLNode* node){
	if(node != NULL){
		DeleteSkipList(node->next[0]);
		delete[] node->next;
		delete node;
	}
}

// 6.PrintSkipList()
void PrintSkipList(SkipList* list){
	SLNode* node = list->head->next[0];
	while(node != NULL){
		cout << "Key: " << node->key << "  Value:" << node->value << endl;
		node = node->next[0];
	}
}

// 7.GetHeight()
int GetHeight(int maxHeight){
	int height = 1;
	double p = 0.5;
	srand(time(NULL));
	for(double x = rand()/INT_MAX; height < maxHeight and x < p; x = rand()/INT_MAX)
		height++;
	return height;
}

// 8.AddSLNode()
// a) by key
void AddSLNode(SkipList* list, int pKey){
	AddSLNode(list, pKey, -1);
}

// b) by key and value
void AddSLNode(SkipList* list, int pKey, int pValue){
	SLNode* tmp = list->head;
	int height = GetHeight(list->height);
	SLNode* node = new SLNode();
	node->value = pValue;
	node->key = pKey;
	node->next = new SLNode*[height];

	for(int i = list->height - 1; i >= 0; i--){
		while(tmp->next[i] != NULL and node->key > tmp->next[i]->key)
			tmp = tmp->next[i];
		if(i < height){
			node->next[i] = tmp->next[i];
			tmp->next[i] = node;
		}
	}
}

// 9.FindSLNode()
SLNode* FindSLNode(SkipList* list, int pKey){
	SLNode* tmp = list->head;
	for(int i = list->height - 1; i >= 0; i--)
		while(tmp->next[i] != NULL and pKey > tmp->next[i]->key)
			tmp = tmp->next[i];
	if(tmp->next[0] == NULL or tmp->next[0]->key != pKey)
		return NULL;
	else
		return tmp->next[0];
}

// 10.DeleteSLNode()
void DeleteSLNode(SkipList* list, int pKey){
	SLNode* tmp = list->head;
	SLNode* tmp2;
	for(int i = list->height - 1; i >= 0; i--){
		while(tmp->next[i] != NULL and pKey > tmp->next[i]->key)
			tmp = tmp->next[i];
		if(tmp->next[i] != NULL and tmp->next[i]->key == pKey){
			tmp2 = tmp->next[i];
			tmp->next[i] = tmp->next[i]->next[i];
		}
	}
	delete[] tmp2->next;
	delete tmp2;
}