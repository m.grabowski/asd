// sort.h
#ifndef SORT_H
#define SORT_H

#include <string>
using std::string;

// Operations on arrays
int* AllocateArray(int size);
void PrintArray(int* array, int size);
void FillArray(int* array, int size, int number);
void FillArrayRandomly(int* array, int size);
void DeleteArray(int* array);

// MergeSort
void MergeSort(int* array, int size);
void MergeSort(int* array, int start, int end);
void Merge(int* array, int start, int mid, int end);

// QuickSort
void QuickSort(int* array, int size);
void QuickSort(int* array, int start, int end);
int Partition(int* array, int start, int end);

// HeapSort
int Left(int index);
int Right(int index);
int Parent(int index);
void HeapifyMax(int* array, int size, int index);
void BuildHeapMax(int* array, int size);
void HeapSort(int* array, int size);

// CountingSort
void CountingSort(int* array, int size);
void CountingSort(int* array, int size, int range);

// RadixSort
void RadixSortWords(string* array, int size);

// BubbleSort
void BubbleSort(int* array, int size);

// InsertionSort
void InsertionSort(int* array, int size);

// SelectionSort
int GetMinIndex(int* array, int size, int toIndex);
void SelectionSort(int* array, int size);

#endif