// findUnion.h
#ifndef FINDUNION_H
#define FINDUNION_H

struct Data;
struct Subset;

struct Data{
	int key, value;

	Data();
	Data(int pKey);
	Data(int pKey, int pValue);
	~Data();
};

struct Subset{
	Data d;
	int rank;
	Subset* parent;

	Subset();
	Subset(Data pData);
	~Subset();
};

// MakeSet
void MakeSet(Subset* set);

// Find with path compression
Subset* Find(Subset* set);

// Union by rank
void Union(Subset* set1, Subset* set2);

#endif