#include <iostream>
#include "graph.h"
#include "findUnion.h"
#include "priorityQueue.h"
#include <limits.h>
using namespace std;

/*
   Graph algorithms
1. Graph constructor and destructor
2. Vertex constructor and destructor
3. Edge constructor and destructor
4. CreateGraph()
5. PrintGraph()
6. GraphMatrixToList()
7. GraphListToMatrix()
8. DFS()
9. DFSVisit()
10. BFS()
11. Transposition()
12. GraphDegree()
13. CopyGraph()
14. BuildGraphFromDigraph()
15. Connectivity()
16. ConnectedComponents()
17. StronglyConnectedComponents()
18. Bridges()
19. EulerianCyclePath()
20. MSTPrim()
21. MSTKruskal()
22. Dijkstra()
23. BellmanFord()
24. FloydWarshall()
*/

// 1.Graph constructors and destructor
Graph::Graph(){
	verticesNumber = edgesNumber = 0;
	matrix = NULL;
	list = NULL;
	digraph = true;
}

Graph::Graph(int vertices){
	verticesNumber = vertices;
	edgesNumber = 0;
	matrix = NULL;
	list = NULL;
	digraph = true;
}

Graph::Graph(int vertices, int edges){
	verticesNumber = vertices;
	edgesNumber = edges;
	matrix = NULL;
	list = NULL;
	digraph = true;
}

Graph::Graph(const Graph& G){
	digraph = G.digraph;
	verticesNumber = G.verticesNumber;
	edgesNumber = G.edgesNumber;

	list = NULL;
	matrix = new int*[verticesNumber];
	for(int i = 0; i < verticesNumber; i++)
		matrix[i] = new int[verticesNumber];
	
	if(G.list != NULL){
		for(int i = 0; i < verticesNumber; i++){
			Vertex* tmp = G.list[i];
			while(tmp != NULL){
				matrix[i][tmp->number] = tmp->weight;
				tmp = tmp->next;
			}
		}
	}
	else{
		for(int i = 0; i < verticesNumber; i++)
			for(int j = 0; j < verticesNumber; j++)
				matrix[i][j] = G.matrix[i][j];
	}
}

Graph::~Graph(){
	if(verticesNumber > 0){
		if(matrix != NULL){
			for(int i = 0; i < verticesNumber; i++)
				if(matrix[i] != NULL){
					delete[] matrix[i];
					matrix[i] = NULL;
				}
			delete[] matrix;
			matrix = NULL;
		}
		if(list != NULL){
			for(int i = 0; i < verticesNumber; i++){
				if(list[i] != NULL){
					Vertex* tmp = list[i];
					while(tmp != NULL){
						Vertex* tmp2 = tmp;
						tmp = tmp->next;
						delete tmp2;
					}
				}
			}
			delete[] list;
			list = NULL;
		}
		verticesNumber = edgesNumber = 0;
	}
}

// 2.Vertex constructors and destructor
Vertex::Vertex(){
	number = -1;
	weight = 0;
	next = NULL;
}

Vertex::Vertex(int index){
	if(index >= 0)
		number = index;
	else
		number = -1;
	weight = 0;
	next = NULL;
}

Vertex::Vertex(int index, int pWeight){
	if(index >= 0)
		number = index;
	else 
		number = -1;
	weight = pWeight;
	next = NULL;
}

Vertex::~Vertex(){}

// 3.Edge constructors and destructor
Edge::Edge(){
	vertex1 = vertex2 = -1;
	weight = 0;
}

Edge::Edge(int index1, int index2){
	if(index1 >= 0 and index2 >= 0){
		vertex1 = index1;
		vertex2 = index2;
		weight = 1;
	}
	else{
		weight = 0;
		vertex1 = vertex2 = -1;
	}
}

Edge::Edge(int index1, int index2, int pWeight){
	if(index1 >= 0 and index2 >= 0){
		vertex1 = index1;
		vertex2 = index2;
		weight = pWeight;
	}
	else{
		weight = 0;
		vertex1 = vertex2 = -1;
	}
}

Edge::~Edge(){}

// 4.CreateGraph()
void CreateGraph(Graph& G, int vertices, int edges, bool version){ // if version == false => then use adjacency matrix, else use adjacency list
	if(version){
		if(vertices > 0 and edges >= 0){
			G.verticesNumber = vertices;
			G.edgesNumber = edges;
			G.list = new Vertex*[G.verticesNumber];
			for(int i = 0; i < G.verticesNumber; i++)
				G.list[i] = NULL;

			if(edges > 0)
				cout << "Enter " << edges << " edges (vertex, vertex, weight)" << endl;
			int tmp1, tmp2, tmp3;
			for(int i = 0; i < edges; i++){
				cin >> tmp1 >> tmp2 >> tmp3;
				if(tmp1 < vertices and tmp1 >=0 and tmp2 < vertices and tmp2 >= 0 and tmp3 != 0){
					Vertex* tmp = new Vertex(tmp2, tmp3);
					tmp->next = G.list[tmp1];
					G.list[tmp1] = tmp;
				}
			}
		}
		else 
			return;
	}
	else{
		if(vertices > 0 and edges >= 0){
			G.verticesNumber = vertices;
			G.edgesNumber = edges;
			G.matrix = new int*[G.verticesNumber];
			for(int i = 0; i < G.verticesNumber; i++)
				G.matrix[i] = new int[G.verticesNumber];
			for(int i = 0; i < G.verticesNumber; i++)
				for(int j = 0; j < G.verticesNumber; j++)
					G.matrix[i][j] = 0;

			if(edges > 0)
				cout << "Enter " << edges << " edges (vertex, vertex, weight)" << endl;
			int tmp1, tmp2, tmp3;
			for(int i = 0; i < edges; i++){
				cin >> tmp1 >> tmp2 >> tmp3;
				if(tmp1 < vertices and tmp1 >= 0 and tmp2 < vertices and tmp2 >= 0)
					G.matrix[tmp1][tmp2] = tmp3;
			}
		}
		else
			return;
	}
}

// 5.PrintGraph
void PrintGraph(Graph& G){
	if(G.verticesNumber > 0){
		if(G.matrix != NULL){
			for(int i = 0; i < G.verticesNumber; i++){
				cout << "Vertex nr: " << i << " adjacent vertices: ";
				for(int j = 0; j < G.verticesNumber; j++){
					if(G.matrix[i][j] != 0)
						cout << j << " ";
				}
				cout << endl;
			}
		}
		else if(G.list != NULL){
			for(int i = 0; i < G.verticesNumber; i++){
				cout << "Vertex nr: " << i << " adjacent vertices: ";
				Vertex* tmp = G.list[i];
				while(tmp != NULL){
					cout << tmp->number << " ";
					tmp = tmp->next;
				}
				cout << endl;
			}	
		}
		else
			cout << "There are no edges in this graph" << endl;
	}	
}

// 6.GraphMatrixToList()
void GraphMatrixToList(Graph& G){
	if(G.verticesNumber > 0){
		if(G.matrix == NULL){
			cout << "Cannot convert graph representation from adj. matrix do adj. list" << endl;
			return;
		}
		else if(G.list != NULL){
			cout << "This graph already has an adj. list" << endl;
			return;
		}
		else{
			G.list = new Vertex*[G.verticesNumber];
			for(int i = 0; i < G.verticesNumber; i++)
				G.list[i] = NULL;

			for(int i = 0; i < G.verticesNumber; i++){
				Vertex* tmp;
				for(int j = 0; j < G.verticesNumber; j++){
					if(G.matrix[i][j] != 0){
						tmp = new Vertex(j, G.matrix[i][j]);
						tmp->next = G.list[i];
						G.list[i] = tmp;
					}		
				}
			}

			for(int i = 0; i < G.verticesNumber; i++) 					// remove this if having both representations is required
				delete[] G.matrix[i];
			delete[] G.matrix; 
			G.matrix = NULL;
		}
	}
}

// 7.GraphListToMatrix(Graph& G)
void GraphListToMatrix(Graph& G){
	if(G.verticesNumber > 0){
		if(G.list == NULL){
			cout << "Cannot convert graph representation from adj. list to adj. matrix" << endl;
			return;
		}
		else if(G.matrix != NULL){
			cout << "This graph already has an adj. matrix" << endl;
			return;
		}
		else{
			G.matrix = new int*[G.verticesNumber];
			for(int i = 0; i < G.verticesNumber; i++)
				G.matrix[i] = new int[G.verticesNumber];

			for(int i = 0; i < G.verticesNumber; i++)
				for(int j = 0; j < G.verticesNumber; j++)
					G.matrix[i][j] = 0;

			for(int i = 0; i < G.verticesNumber; i++){
				Vertex* tmp = G.list[i];
				while(tmp != NULL){
					G.matrix[i][tmp->number] = tmp->weight;
					tmp = tmp->next;
				}
			}

			for(int i = 0; i < G.verticesNumber; i++){ 					// remove this if having both representations is required
				while(G.list[i] != NULL){
					Vertex* tmp = G.list[i];
					G.list[i] = G.list[i]->next;
					delete tmp;
				}
			}
			delete[] G.list;
			G.list = NULL;
		}
	}
}

// 8.DFS()
// a) with no start vertex
void DFS(Graph& G){
	bool* visited = new bool[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++)
		visited[i] = false;
	
	int* enterTime = new int[G.verticesNumber];
	int* finishTime = new int [G.verticesNumber];
	int* parent = new int[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++)
		enterTime[i] = finishTime[i] = parent[i] = -1;

	int time = 0;
	for(int i = 0; i < G.verticesNumber; i++)
		if(!visited[i])
			DFSVisit(G, i, time, enterTime, finishTime, parent, visited);

	delete[] visited;
	delete[] enterTime;
	delete[] finishTime;
	delete[] parent;
}

// b) with a start vertex
void DFS(Graph& G, int start){
	bool* visited = new bool[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++)
		visited[i] = false;
	
	int* enterTime = new int[G.verticesNumber];
	int* finishTime = new int [G.verticesNumber];
	int* parent = new int[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++)
		enterTime[i] = finishTime[i] = parent[i] = -1;

	int time = 0;
	DFSVisit(G, start, time, enterTime, finishTime, parent, visited);

	//for(int i = 0; i < G.verticesNumber; i++)
	//	cout << "Vertex nr: " << i << " enterTime: " << enterTime[i] << " finishTime: " << finishTime[i] << " parent[i]: " << parent[i] << " visited: " << visited[i] << endl;

	delete[] visited;
	delete[] enterTime;
	delete[] finishTime;
	delete[] parent;
}

// 9.DFSVisit()
void DFSVisit(Graph& G, int vertex, int& time, int* enterTime, int* finishTime, int* parent, bool* visited){
	time++;
	enterTime[vertex] = time;
	visited[vertex] = true;
	if(G.list != NULL){
		Vertex* tmp = G.list[vertex];
		while(tmp != NULL){
			if(!visited[tmp->number]){
				parent[tmp->number] = vertex;
				DFSVisit(G, tmp->number, time, enterTime, finishTime, parent, visited);
			}
			tmp = tmp->next;
		}
	}
	else{
		for(int i = 0; i < G.verticesNumber; i++){
			if(G.matrix[vertex][i] != 0 and !visited[i]){
				parent[i] = vertex;
				DFSVisit(G, i, time, enterTime, finishTime, parent, visited);
			}
		}
	}
	time++;
	finishTime[vertex] = time;
}

// 10.BFS()
void BFS(Graph& G, int start){
	bool* visited = new bool[G.verticesNumber];
	int* distance = new int[G.verticesNumber];
	int* parent = new int[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++){
		visited[i] = false;
		distance[i] = parent[i] = -1;
	}
	visited[start] = true;
	distance[start] = 0;
	parent[start] = -1;

	int* queue = new int[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++)
		queue[i] = -1;
	int queueSize = 0;
	int queueFirst = 0;
	int queuePtr = 0;
	queue[queuePtr] = start;
	queuePtr++;
	queueSize++;
	while(queueSize > 0){
		int vertex = queue[queueFirst];
		queueSize--;
		queueFirst++;
		if(G.list != NULL){
			Vertex* tmp = G.list[vertex];
			while(tmp != NULL){
				int nr = G.list[vertex]->number;
				if(!visited[nr]){
					visited[nr] = true;
					distance[nr] = distance[vertex] + 1;
					parent[nr] = vertex;
					queue[queuePtr] = nr;
					queuePtr++;
					queueSize++;
				}
				tmp = tmp->next;
			}
		}
		else{
			for(int i = 0; i < G.verticesNumber; i++){
				if(G.matrix[vertex][i] != 0 and !visited[i]){
					visited[i] = true;
					distance[i] = distance[vertex] + 1;
					parent[i] = vertex;
					queue[queuePtr] = i;
					queuePtr++;
					queueSize++;
				}
			}
		}
	}

	//for(int i = 0; i < G.verticesNumber; i++)
	//	cout << "Vertex i: " << i << " distance: " << distance[i] << " parent: " << parent[i] << endl;
	delete[] visited;
	delete[] distance;
	delete[] parent;
}

// 11.Transposition()
void Transposition(Graph& G){
	if(G.list != NULL){ 												// for adjacency list
		Vertex** tmpArray = new Vertex*[G.verticesNumber];
		for(int i = 0; i < G.verticesNumber; i++)
			tmpArray[i] = NULL;

		for(int i = 0; i < G.verticesNumber; i++){
			Vertex* tmp = G.list[i];
			while(tmp != NULL){
				Vertex* tmp2 = new Vertex(i, tmp->weight);
				tmp2->next = tmpArray[tmp->number];
				tmpArray[tmp->number] = tmp2;
				tmp = tmp->next;
			}
			tmp = G.list[i];
			while(tmp != NULL){
				Vertex* r = tmp;
				tmp = tmp->next;
				delete r;
			}
		}

		for(int i = 0; i < G.verticesNumber; i++)
			G.list[i] = tmpArray[i];

		delete[] tmpArray;
	}
	else{ 																// for adjacency matrix
		for(int i = 0; i < G.verticesNumber; i++){
			for(int j = i; j < G.verticesNumber; j++){
				swap(G.matrix[i][j], G.matrix[j][i]);
			}
		}
	}
}

// 12.GraphDegree()
int GraphDegree(Graph& G){
	if(G.verticesNumber > 0){
		int degree = 0;
		if(G.matrix != NULL){
			for(int i = 0; i < G.verticesNumber; i++){
				int maxdg = 0;
				for(int j = 0; j < G.verticesNumber; j++){
					if(G.matrix[i][j] != 0)
						maxdg++;
				}
				if(maxdg > degree)
					degree = maxdg;
			}
		}
		else if(G.list != NULL){
			for(int i = 0; i < G.verticesNumber; i++){
				int maxdg = 0;
				Vertex* tmp = G.list[i];
				while(tmp != NULL){
					maxdg++;
					tmp = tmp->next;
				}
				if(maxdg > degree)
					degree = maxdg;
			}	
		}
		return degree;
	}
	else 
		return 0;
}

// 13.CopyGraph()
void CopyGraph(Graph& G, Graph& newGraph){
	bool list = false;
	if(G.matrix == NULL){
		list = true;
		GraphListToMatrix(G);
	}
	newGraph.digraph = G.digraph;
	newGraph.verticesNumber = G.verticesNumber;
	newGraph.edgesNumber = G.edgesNumber;
	newGraph.list = NULL;
	newGraph.matrix = new int*[newGraph.verticesNumber];
	for(int i = 0; i < newGraph.verticesNumber; i++)
		newGraph.matrix[i] = new int[newGraph.verticesNumber];

	for(int i = 0; i < newGraph.verticesNumber; i++)
		for(int j = 0; j < newGraph.verticesNumber; j++)
			newGraph.matrix[i][j] = G.matrix[i][j];

	if(list)
		GraphMatrixToList(G);
}

// 14.BuildGraphFromDigraph()
Graph BuildGraphFromDigraph(Graph& G){ 					
	Graph newGraph;
	if(G.digraph){
		bool list = false;
		if(G.list != NULL)
			list = true;
		CopyGraph(G, newGraph);
		if(list)
			GraphListToMatrix(G);
		newGraph.digraph = false;
		for(int i = 0; i < newGraph.verticesNumber; i++)
			for(int j = i; j < newGraph.verticesNumber; j++)
				if(newGraph.matrix[i][j] != 0)
					newGraph.matrix[j][i] = newGraph.matrix[i][j];
				else if(newGraph.matrix[j][i] != 0)
					newGraph.matrix[i][j] = newGraph.matrix[j][i];
		if(list)
			GraphMatrixToList(G);
	}
	else
		CopyGraph(G, newGraph);
	return newGraph;
}

// 15.Connectivity()
bool Connectivity(Graph& G){
	bool isConnected = false;
	if(G.verticesNumber <= 0)
		isConnected = false;
	else if(G.digraph){
		Graph newGraph = BuildGraphFromDigraph(G);		
		isConnected = Connectivity(newGraph);
	}
	else{
		bool* visited = new bool[G.verticesNumber];
		for(int i = 0; i < G.verticesNumber; i++)
			visited[i] = false;
	
		int* enterTime = new int[G.verticesNumber];
		int* finishTime = new int [G.verticesNumber];
		int* parent = new int[G.verticesNumber];
		for(int i = 0; i < G.verticesNumber; i++)
			enterTime[i] = finishTime[i] = parent[i] = -1;

		int time = 0;
		int numberOfComponents = 0;
		for(int i = 0; i < G.verticesNumber; i++)
			if(!visited[i]){
				numberOfComponents++;
				DFSVisit(G, i, time, enterTime, finishTime, parent, visited);
			}

		delete[] visited;
		delete[] enterTime;
		delete[] finishTime;
		delete[] parent;
		
		if(numberOfComponents == 1)
			isConnected = true;
	}
	return isConnected;
}

// 16.ConnectedComponents()
// a) whole graph
void ConnectedComponents(Graph& G){
	bool list = false;
	if(G.list != NULL){
		list = true;
		GraphListToMatrix(G);
	}
	int componentCounter = 0;
	int* component = new int[G.verticesNumber];
	bool* visited = new bool[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++){
		visited[i] = false;
		component[i] = -1;
	}

	for(int i = 0; i < G.verticesNumber; i++){
		if(!visited[i]){
			componentCounter++;
			component[i] = componentCounter;
			ConnectedComponentsDFS(G, i, component, visited);
		}
	}
	cout << G.verticesNumber << endl;
	for(int i = 0; i < G.verticesNumber; i++)
		cout << "Vertex number: " << i << " component number: " << component[i] << endl;

	if(list)
		GraphMatrixToList(G);

	delete[] component;
	delete[] visited;
}

// b) DFS procedure used in ConnectedComponents()
void ConnectedComponentsDFS(Graph& G, int vertex, int* component, bool* visited){
	visited[vertex] = true;
	for(int i = 0; i < G.verticesNumber; i++){
		if(G.matrix[vertex][i] != 0 and !visited[i]){
			component[i] = component[vertex];
			ConnectedComponentsDFS(G, i, component, visited);
		}
	}
}

// 17.StronglyConnectedComponents()
// a) main function
void StronglyConnectedComponents(Graph& G){ 							// to maintain O(V+E) do it on adjacency lists, now it's O(V^2) 
	int time = 0;
	bool list = false;
	Vertex* finishTime = NULL; 											// on this stack, in number = number of vertex, weight = time of processing
	bool* visited = new bool[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++)
		visited[i] = false;

	if(G.list != NULL){
		list = true;
		GraphListToMatrix(G);
	}

	for(int i = 0; i < G.verticesNumber; i++) 							// DFS with timestack
		if(!visited[i])
			StronglyConnectedComponentsDFS(G, i, time, finishTime, visited);

	Transposition(G); 													// transposition

	int componentCounter = 0; 											// prepare for second time DFS
	int* component = new int[G.verticesNumber]; 						
	for(int i = 0; i < G.verticesNumber; i++){
		component[i] = 0;
		visited[i] = false;
	}

	for(Vertex* tmp = finishTime; tmp != NULL; tmp = tmp->next){
		int currentVertex = tmp->number;
		if(!visited[currentVertex]){
			componentCounter++;
			component[currentVertex] = componentCounter;
			ConnectedComponentsDFS(G, tmp->number, component, visited);
		}
	}

	for(int i = 0; i < G.verticesNumber; i++) 							// print components
		cout << "Vertex nr: " << i << " SCC nr: " << component[i] << endl;

	delete[] visited;
	delete[] component;
	while(finishTime != NULL){
		Vertex* tmp = finishTime;
		finishTime = finishTime->next;
		delete tmp;
	}
	Transposition(G); 													// transpose graph back
	if(list)
		GraphMatrixToList(G);
}

// b) one of DFSs used in StronglyConnectedComponets
void StronglyConnectedComponentsDFS(Graph& G, int vertex, int& time, Vertex* &finishTime, bool* visited){
	time++;
	visited[vertex] = true;
	for(int i = 0; i < G.verticesNumber; i++){
		if(G.matrix[vertex][i] != 0 and !visited[i]){
			StronglyConnectedComponentsDFS(G, i, time, finishTime, visited);
		}
	}
	time++;
	Vertex* tmp = new Vertex(vertex, time);
	tmp->next = finishTime;
	finishTime = tmp;
}

// 18.FindBridges()
// a) main procedure
void FindBridges(Graph& G){
	bool list = false;
	if(G.list != NULL){
		GraphListToMatrix(G);
		list = true;
	}

	int vertexCounter = 0;
	int* vertexNumber = new int[G.verticesNumber];
	bool* visited = new bool[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++){
		vertexNumber[i] = 0;
		visited[i] = false;
	}

	for(int i = 0; i < G.verticesNumber; i++){
		if(!visited[i]){
			vertexCounter = 1;
			int k = FindBridgesDFS(G, i, -1, vertexNumber, visited, vertexCounter);
		}
	}

	if(list)
		GraphMatrixToList(G);

	delete[] vertexNumber;
	delete[] visited;
}

// b) DFS used in main procedure
int FindBridgesDFS(Graph& G, int vertex, int parent, int* vertexNumber, bool* visited, int& vertexCounter){
	int low;
	vertexNumber[vertex] = low = vertexCounter;
	visited[vertex] = true;
	vertexCounter++;
	for(int i = 0; i < G.verticesNumber; i++){
		if(i != parent and G.matrix[vertex][i] != 0){
			if(!visited[i]){
				int tmp = FindBridgesDFS(G, i, vertex, vertexNumber, visited, vertexCounter);
				if(tmp < low)
					low = tmp;
			}
			else if(vertexNumber[i] < low)
				low = vertexNumber[i];
		}
	}

	if(parent != -1 and low == vertexNumber[vertex]){
		cout << "Bridge from vertex " << vertex << " to vertex " << parent << endl;
	}
	return low;
}

// 19.EulerianCyclePath
// a) main procedure
void EulerianCyclePath(Graph& G){
	if(G.verticesNumber > 0){
		if(!G.digraph){
			bool list = false;
			if(G.list != NULL){
				list = true;
				GraphListToMatrix(G);
			}

			bool* visited = new bool[G.verticesNumber];
			int oddCounter = 0;
			int components = 0;
			for(int i = 0; i < G.verticesNumber; i++){
				if(!visited[i]){
					components++;
					EulerianCyclePathDFS(G, i, visited, oddCounter);
				}
			}
			if(components == 1){
				if(oddCounter == 0)
					cout << "Graph has an eulerian cycle" << endl;
				else if(oddCounter == 2)
					cout << "Graph has an eulerian path" << endl;	
				else
					cout << "Graph has neither eulerian cycle nor eulerian path in this graph" << endl;
			}
			else
				cout << "Graph has neither eulerian cycle nor eulerian path in this graph" << endl;

			delete[] visited;
			if(list)
				GraphMatrixToList(G);
		}
		else{
			int time = 0;
			bool list = false;
			bool connected = Connectivity(G);
			Vertex* finishTime = NULL; 											// on this stack, in number = number of vertex, weight = time of processing
			bool* visited = new bool[G.verticesNumber];
			for(int i = 0; i < G.verticesNumber; i++)
				visited[i] = false;

			if(G.list != NULL){
				list = true;
				GraphListToMatrix(G);
			}

			for(int i = 0; i < G.verticesNumber; i++) 							// DFS with timestack
				if(!visited[i])
					StronglyConnectedComponentsDFS(G, i, time, finishTime, visited);

			Transposition(G); 													// transposition

			int componentCounter = 0; 											// prepare for second time DFS
			int* component = new int[G.verticesNumber]; 						
			for(int i = 0; i < G.verticesNumber; i++){
				component[i] = -1;
				visited[i] = false;
			}

			for(Vertex* tmp = finishTime; tmp != NULL; tmp = tmp->next){ 		// DFS according to timestack
					if(!visited[tmp->number]){
						componentCounter++;
						component[tmp->number] = componentCounter;
						ConnectedComponentsDFS(G, tmp->number, component, visited);
					}
			}
	

			int mainComponent = component[0];
			int in, out;
			in = out = 0;
			int edgesIn, edgesOut;
			bool fail = false;
			for(int i = 0; i < G.verticesNumber; i++){
				edgesIn = edgesOut = 0;
				if(component[i] != mainComponent){
					if(!connected){
						cout << "There is neither eulerian cycle nor eulerian path in this graph" << endl;
						fail = true;
						break;
					}
				}
				else if(!connected){
					cout << "There is no eulerian path in this graph" << endl;
					fail = true;
					break;
				}
				else{
					for(int j = 0; j < G.verticesNumber; j++){
						if(G.matrix[i][j] != 0)
							edgesOut++;
						else if(G.matrix[j][i] != 0)
							edgesIn++;
					}
					if(edgesOut - edgesIn == 1)
						out++;
					else if(edgesIn - edgesOut == 1)
						in++;
					else if(edgesOut != edgesIn){
						cout << "There is neither eulerian cycle nor eulerian path in this graph" << endl;
						fail = true;
						break;
					}
					if(out > 1 or in > 1){
						cout << "There is neither eulerian cycle nor eulerian path in this graph" << endl;
						fail = true;
						break;
					}
					
				}
			}					
			if(in == 1 and !fail)
				cout << "There is an eulerian path in this graph" << endl;
			else if(!fail)
				cout << "There is an eulerian cycle in this graph" << endl;

			delete[] visited;
			delete[] component;
			while(finishTime != NULL){
				Vertex* tmp = finishTime;
				finishTime = finishTime->next;
				delete tmp;
			}
			Transposition(G); 													
			if(list)
				GraphMatrixToList(G);
		}
	}
	else 
		cout << "There is neither eulerian cycle nor eulerian path in this graph" << endl;
}

// b) subprocedure
void EulerianCyclePathDFS(Graph& G, int vertex, bool* visited, int& oddCounter){
	int degree = 0;
	visited[vertex] = true;
	for(int i = 0; i < G.verticesNumber; i++){
		if(G.matrix[vertex][i] != 0){
			degree++;
			if(!visited[i])
				EulerianCyclePathDFS(G, i, visited, oddCounter);
		}
	}
	if(degree % 2 == 1)
		oddCounter++;
}

// 20.MSTPrim()
// a) main procedure
int MSTPrim(Graph& G){
	if(!Connectivity(G)){
		cout << "Graph is not connected, cannot find MST" << endl;
		return -1;
	}
	Graph newGraph = BuildGraphFromDigraph(G);

	int* parent = new int[G.verticesNumber];
	int* key = new int[G.verticesNumber];
	bool* visited = new bool[G.verticesNumber];

	for(int i = 0; i < G.verticesNumber; i++){
		parent[i] = -1;
		key[i] = INT_MAX;
		visited[i] = false;
	}

	int weight = MSTPrim(newGraph, 0, parent, key, visited);
	delete[] parent;
	delete[] key;
	delete[] visited;
	return weight;
}

// b) subprocedure
int MSTPrim(Graph& G, int startVertex, int* parent, int* key, bool* visited){
	key[startVertex] = 0;
	PriorityQueue Q(1);
	for(int i = 0; i < G.verticesNumber; i++){
		T tmp(key[i], i);
		AddElement(Q, tmp);
	}

	while(!QueueEmpty(Q)){
		T tmp = RemoveMin(Q);
		int currentVertex = tmp.value;
		//cout << "Adding edge " << parent[currentVertex] << " to " << currentVertex << " weight: " << tmp.key << endl; 
		visited[currentVertex] = true;
		for(int i = 0; i < G.verticesNumber; i++){
			if(G.matrix[currentVertex][i] != 0 and !visited[i]){
				if(G.matrix[currentVertex][i] < key[i]){
					parent[i] = currentVertex;
					key[i] = G.matrix[currentVertex][i];
					T tmp2(key[i], i);
					AddElement(Q, tmp2);
				}
			}
		}
	}

	int MSTweight = 0;
	for(int i = 0; i < G.verticesNumber; i++){
		MSTweight += key[i];
		cout << "vertex nr: " << i << " key: " << key[i] << " parent: " << parent[i] << endl; 
	}
	return MSTweight;	
}

// 21.MSTKruskal()
// a) main procedure
int MSTKruskal(Graph& G){
	if(!Connectivity(G)){
		cout << "Graph is not connected, cannot find MST" << endl;
		return -1;
	}
	Graph newGraph = BuildGraphFromDigraph(G);
	int weight = 0;
	Subset** vertices = new Subset*[newGraph.verticesNumber];
	for(int i = 0; i < newGraph.verticesNumber; i++){
		Data tmp(i);
		vertices[i] = new Subset(tmp);
		MakeSet(vertices[i]);
	}
	Edge* array = new Edge[newGraph.edgesNumber];
	int edges = 0;
	for(int i = 0; i < newGraph.verticesNumber; i++){
		for(int j = i; j < newGraph.verticesNumber; j++){
			if(newGraph.matrix[i][j] != 0){
				Edge tmp(i, j, newGraph.matrix[i][j]);
				array[edges] = tmp;
				edges++;
			}
		}
	}
	SortEdges(array, edges);
	for(int i = 0; i < edges; i++){
		if(Find(vertices[array[i].vertex1]) != Find(vertices[array[i].vertex2])){
			//cout << "Adding edge " << array[i].vertex1 << " to " << array[i].vertex2 << " weight: " << array[i].weight << endl;
			weight += array[i].weight;
			Union(vertices[array[i].vertex1], vertices[array[i].vertex2]);
		}
	}

	for(int i = 0; i < newGraph.verticesNumber; i++)
		delete vertices[i];
	delete[] vertices;
	delete[] array;
	return weight;
}

// b) edge sorting procedure for MSTKruskal()
void SortEdges(Edge* array, int size){
	EdgeQS(array, 0, size -1);
}

// c) edge sorting subprocedure #1
void EdgeQS(Edge* array, int start, int end){
	if(start < end){
		int pivot = EdgePartition(array, start, end);
		EdgeQS(array, start, pivot-1);
		EdgeQS(array, pivot+1, end);
	}
}

// d) edge sorting subprocedure #2
int EdgePartition(Edge* array, int start, int end){
	int pivot = array[end].weight;
	int i = start -1;
	for(int j = start; j < end; j++){
		if(array[j].weight <= pivot){
			i++;
			swap(array[i], array[j]);
		}
	}
	swap(array[i + 1], array[end]);
	return i + 1;
}

// 22.Dijkstra()
void Dijkstra(Graph& G, int start){
	if(!Connectivity(G)){
		cout << "Cannot find shortest paths in disconnnected graph" << endl;
		return;
	}
	bool list = false;
	if(G.list != NULL){
		list = true;
		GraphListToMatrix(G);
	}
	int* distance = new int[G.verticesNumber];
	int* parent = new int[G.verticesNumber];
	bool* visited = new bool[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++){
		distance[i] = INT_MAX;
		parent[i] = -1;
		visited[i] = false;
	}

	distance[start] = 0;
	PriorityQueue Q(1);
	for(int i = 0; i < G.verticesNumber; i++){
		T tmp(distance[i], i);
		AddElement(Q, tmp);
	}

	while(!QueueEmpty(Q)){
		T tmp = RemoveMin(Q);
		int currentVertex = tmp.value;
		visited[currentVertex] = true;

		for(int i = 0; i < G.verticesNumber; i++){
			if(G.matrix[currentVertex][i] != 0 and !visited[i]){
				if(distance[i] > distance[currentVertex] + G.matrix[currentVertex][i]){
					distance[i] = distance[currentVertex] + G.matrix[currentVertex][i];
					parent[i] = currentVertex;
					T temp(distance[i], i);
					AddElement(Q, temp);
				}
			}
		}
	}

	for(int i = 0; i < G.verticesNumber; i++)
		cout << "vertex nr: " << i << " distance: " << distance[i] << " parent: " << parent[i] << endl; 

	if(list)
		GraphMatrixToList(G);
	delete[] distance;
	delete[] parent;
	delete[] visited;
}

// 23.BellmanFord()
bool BellmanFord(Graph& G, int start){
	if(!Connectivity(G)){
		cout << "Cannot find shortest paths in disconnnected graph" << endl;
		return false;
	}
	bool list = false;
	if(G.list != NULL){
		list = true;
		GraphListToMatrix(G);
	}
	int* distance = new int[G.verticesNumber];
	int* parent = new int[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++){
		distance[i] = 1000000; 											// with INT_MAX overflow occurs in relaxation
		parent[i] = -1;
	}
	distance[start] = 0;

	for(int i = 0; i < G.verticesNumber -1; i++){ 						// relaxation loop
		for(int j = 0; j < G.verticesNumber; j++){
			for(int k = 0; k < G.verticesNumber; k++){
				if(G.matrix[j][k] != 0){
					if(distance[k] > distance[j] + G.matrix[j][k]){
						distance[k] = distance[j] + G.matrix[j][k];
						parent[k] = j;
					}
				}
			}
		}
	}

	bool negative = false; 												// search for negative cycle in loop below
	for(int i = 0; i < G.verticesNumber; i++){
		for(int j = 0; j < G.verticesNumber; j++){
			if(G.matrix[i][j] != 0){
				if(distance[j] > distance[i] + G.matrix[i][j]){
					cout << "Negative cycle found" << endl;
					negative = true;
				}
			}
		}
	}

	for(int i = 0; !negative and i < G.verticesNumber; i++)
		cout << "vertex nr: " << i << " distance: " << distance[i] << " parent: " << parent[i] << endl; 

	if(list)
		GraphMatrixToList(G);

	delete[] distance;
	delete[] parent;
	
	return !negative;
}

// 24.FloydWarshall()
// a) main procedure
void FloydWarshall(Graph& G){
	if(!Connectivity(G)){
		cout << "Cannot find shortest paths in disconnnected graph" << endl;
		return;
	}
	bool list = false;
	if(G.list != NULL){
		list = true;
		GraphListToMatrix(G);
	}
	int** distance = new int*[G.verticesNumber];
	int** parent = new int*[G.verticesNumber];
	for(int i = 0; i < G.verticesNumber; i++){
		parent[i] = new int[G.verticesNumber];
		distance[i] = new int[G.verticesNumber];
	}

	for(int i = 0; i < G.verticesNumber; i++)
		for(int j = 0; j < G.verticesNumber; j++){
			parent[i][j] = -1;
			distance[i][j] = 1000000;
		}

	for(int i = 0; i < G.verticesNumber; i++){
		distance[i][i] = 0;
		for(int j = 0; j < G.verticesNumber; j++)
			if(G.matrix[i][j] != 0){
				parent[i][j] = i;
				distance[i][j] = G.matrix[i][j];
			}
	}

	for(int i = 0; i < G.verticesNumber; i++){
		for(int j = 0; j < G.verticesNumber; j++){
			for(int k = 0; k < G.verticesNumber; k++){
				if(distance[j][k] > distance[j][i] + distance[i][k]){
					distance[j][k] = distance[j][i] + distance[i][k];
					parent[j][k] = parent[i][k];
				}
			}
		}
	}


	for(int i = 0; i < G.verticesNumber; i++)
		for(int j = 0; j < G.verticesNumber; j++){
			cout << "Distance from " << i << " to " << j << " is: " << distance[i][j] << endl;
			PrintFloydWarshallPath(i, j, parent);
			cout << endl;
		}

	if(list)
		GraphMatrixToList(G);
	delete[] distance;
}
// b) recursive function for path reconstruction
void PrintFloydWarshallPath(int start, int end, int** parent){
	if(start == end){
		cout << start << " ";
		return;
	}
	if(parent[start][end] == -1){
		cout << "NO PATH" << endl;
		return;
	}
	PrintFloydWarshallPath(start, parent[start][end], parent);
	cout << end << " ";
}