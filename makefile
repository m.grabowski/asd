# variables
CXX=g++
CXXFLAGS=-c -Wall
OBJECTS=main.o sort.o linkedLists.o skipList.o priorityQueue.o findUnion.o bst.o graph.o

main: $(OBJECTS)
	$(CXX) $(OBJECTS) -o main

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $^
	
clean:
	rm -rf *.o main
