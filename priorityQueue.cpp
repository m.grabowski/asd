#include <iostream>
#include <cstdlib>
#include "limits.h"
#include "priorityQueue.h"
#include "sort.h"
using namespace std;

/*
   Priority Queue
1. T constructors and destructor
2. Swap(T,T);
3. PriorityQueue constructors and destructor
4. MinHeap procedures: (left,right and parent derived from sort.h)
   a) HeapifyMin()
   b) BuildHeapMin()
5. ResizePriorityQueue()
6. AddElement()
7. FirstElement()
8. RemoveMin()
9. QueueEmpty()
*/

// 1.T constructors and destructor
T::T(){}

T::T(int pKey){
	key = pKey;
	value = -1;	
}

T::T(int pKey, int pValue){
	key = pKey;
	value = pValue;
}

T::~T(){}

// 2.Swap(T, T);
void Swap(T& x, T& y){
	T tmp = x;
	x = y;
	y = tmp;
}

// 3.PriorityQueue constructors and destructor
PriorityQueue::PriorityQueue(){
	queueArray = NULL;
	arraySize = 0;
	realSize = 0;
	max = INT_MAX;
}

PriorityQueue::PriorityQueue(int size){
	if(size > 0){
		queueArray = new T[size];
		arraySize = size;
		realSize = 0;
		max = INT_MAX;
	}
	else{
		queueArray = NULL;
		arraySize = 0;
		realSize = 0;
		max = INT_MAX;
	}
}

PriorityQueue::PriorityQueue(const PriorityQueue& queue){
	if(queue.queueArray != NULL){
		queueArray = new T[queue.arraySize];
		arraySize = queue.arraySize;
		for(int i = 0; i < arraySize; i++)
			queueArray[i] = queue.queueArray[i];
		realSize = queue.realSize;
		max = queue.max;
	}
	else{
		queueArray = NULL;
		arraySize = realSize = 0;
		max = queue.max;
	}
}


PriorityQueue::~PriorityQueue(){
	if(queueArray != NULL)
		delete[] queueArray;
}

// 4.MinHeap procedures
// a) HeapifyMin()
void HeapifyMin(PriorityQueue& Q, int index){
	int left = Left(index);
	int right = Right(index);
	int smallest = index;
	if(left < Q.arraySize and Q.queueArray[left].key < Q.queueArray[index].key)
		smallest = left;
	if(right < Q.arraySize and Q.queueArray[right].key < Q.queueArray[smallest].key)
		smallest = right;
	if(smallest != index){
		Swap(Q.queueArray[index], Q.queueArray[smallest]);
		HeapifyMin(Q, smallest);
	}
}

// b) BuildHeapMin()
void BuildHeapMin(PriorityQueue& Q){
	if(Q.arraySize <= 0)
		return;
	for(int i = Q.arraySize / 2; i >= 0; i--)
		HeapifyMin(Q, i);
}

// 5.ResizePriorityQueue()
void ResizePriorityQueue(PriorityQueue& Q){
	if(Q.queueArray == NULL){
		Q.arraySize = 2;
		Q.queueArray = new T[Q.arraySize];
		Q.realSize = 0;
	}
	else{
		T* tmp = new T[Q.arraySize];
		for(int i = 0; i < Q.arraySize; i++)
			tmp[i] = Q.queueArray[i];
		delete[] Q.queueArray;
		Q.arraySize *= 2;
		Q.queueArray = new T[Q.arraySize];
		for(int i = 0; i < Q.realSize; i++)
			Q.queueArray[i] = tmp[i];
		for(int i = Q.realSize; i < Q.arraySize; i++)
			Q.queueArray[i] = Q.max;
		delete[] tmp;
	}
}

// 6.AddElement()
void AddElement(PriorityQueue& Q, T element){
	if(Q.queueArray == NULL or Q.realSize == Q.arraySize)
		ResizePriorityQueue(Q);	
	Q.queueArray[Q.realSize] = element;
	int index = Q.realSize;
	while(index > 0 and Q.queueArray[Parent(index)].key > Q.queueArray[index].key){
		Swap(Q.queueArray[Parent(index)], Q.queueArray[index]);
		index = Parent(index);
	}
	Q.realSize++;
}

// 7.FirstElement()
T FirstElement(PriorityQueue Q){
	if(Q.queueArray == NULL or Q.realSize == 0)
		return T(Q.max, Q.max);
	else{
		return Q.queueArray[0];
	}
}

// 8.RemoveMin()
T RemoveMin(PriorityQueue& Q){
	if(Q.queueArray == NULL or Q.realSize <= 0)
		return T(Q.max, Q.max);
	else{
		T tmp = Q.queueArray[0];
		Q.queueArray[0] = Q.queueArray[Q.realSize - 1];
		HeapifyMin(Q, 0);
		Q.realSize--;
		return tmp;
	}
}

// 9.QueueEmpty()
bool QueueEmpty(PriorityQueue Q){
	if(Q.realSize == 0 or Q.queueArray == NULL)
		return true;
	else
		return false;
}