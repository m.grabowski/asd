#include <iostream>
#include "bst.h"
using namespace std;

/*
   Binary Search Tree
1. BSTNode constructors and destructor
2. BSTree constructor and destructor
3. DeleteTree()
4. Insert()
5. Find()
6. FindMin()
7. FindMax()
8. Successor()
9. Predecessor()
10. DeleteNode()
11. PrintTreePreOrder()
12. PrintTreeInOrder()
13. PrintTreePostOrder()
14. FindKthInOrder()
*/

// 1.BSTNode constructors and destructor
BSTNode::BSTNode(){
	left = right = parent = NULL;
	key = value = -1996;
}

BSTNode::BSTNode(int pKey){
	left = right = parent = NULL;
	key = pKey;
	value = -1;
}

BSTNode::BSTNode(int pKey, int pValue){
	left = right = parent = NULL;
	key = pKey;
	value = pValue;
}

BSTNode::~BSTNode(){}

// 2.BSTree constructor and destructor
BSTree::BSTree(){
	root = NULL;
}

BSTree::~BSTree(){
	DeleteTree(root);
}

// 3.DeleteTree()
// a) in whole tree
void DeleteTree(BSTree* tree){
	if(tree != NULL)
		DeleteTree(tree->root);
	tree->root = NULL;
}

// b) subtree, recursively
void DeleteTree(BSTNode* node){
	if(node == NULL)
		return;
	DeleteTree(node->left);
	DeleteTree(node->right);
	delete node;
}

// 4.Insert()
// a) by passed key and value
void Insert(BSTree* tree, int pKey, int pValue){
	BSTNode* node = new BSTNode(pKey, pValue);
	Insert(tree, node);
}

// b) by passed node
void Insert(BSTree* tree, BSTNode* node){
	BSTNode* x = tree->root;
	BSTNode* y = x;
	while(x != NULL){
		y = x;
		if(x->key < node->key)
			x = x->right;
		else
			x = x->left;
	}

	node->parent = y;
	if(y == NULL){
		tree->root = node;
	}
	else
		if(node->key < y->key)
			y->left = node;
		else
			y->right = node;
}

// 5.Find()
// a) recursively
//    - in whole tree 
BSTNode* FindRecursively(BSTree* tree, int pKey){
	return FindRecursively(tree->root, pKey);
}

//    - subtree
BSTNode* FindRecursively(BSTNode* node, int pKey){
	if(node == NULL or node->key == pKey)
		return node;
	if(pKey < node->key)
		return FindRecursively(node->left, pKey);
	else
		return FindRecursively(node->right, pKey);
}

// b) iteratively
//    - in whole tree
BSTNode* Find(BSTree* tree, int pKey){
	return Find(tree->root, pKey);
}

//    - subtree
BSTNode* Find(BSTNode* node, int pKey){
	while(node != NULL and node->key != pKey)
		if(pKey < node->key)
			node = node->left;
		else
			node = node->right;
	return node;
}

// 6.FindMin()
// a) in whole tree
BSTNode* FindMin(BSTree* tree){
	if(tree == NULL or tree->root == NULL)
		return NULL;
	else
		return FindMin(tree->root);
}

// b) subtree
BSTNode* FindMin(BSTNode* node){
	while(node->left != NULL)
		node = node->left;
	return node;
}

// 7.FindMax()
// a) in whole tree
BSTNode* FindMax(BSTree* tree){
	if(tree == NULL or tree->root == NULL)
		return NULL;
	else
		return FindMax(tree->root);
}

// b) subtree
BSTNode* FindMax(BSTNode* node){
	while(node->right != NULL)
		node = node->right;
	return node;
}

// 8.Successor()
BSTNode* Successor(BSTNode* node){
	if(node == NULL)
		return node;
	else{
		if(node->right != NULL)
			return FindMin(node->right);
		else{
			BSTNode* tmp = node->parent;
			while(tmp != NULL and tmp->left != node){
				node = tmp;
				tmp = tmp->parent;
			}
			return tmp;
		}
	}
}

// 9.Predecessor()
BSTNode* Predecessor(BSTNode* node){
	if(node == NULL)
		return node;
	else{
		if(node->left != NULL)
			return FindMax(node->left);
		else{
			BSTNode* tmp = node->parent;
			while(tmp != NULL and tmp->right != node){
				node = tmp;
				tmp = tmp->parent;
			}
			return tmp;
		}
	}
}

// 10.DeleteNode()
// a) in whole tree
bool DeleteNode(BSTree* tree, int key){
	if(tree == NULL)
		return false;
	else{
		BSTNode* tmp = Find(tree, key);
		if(tmp == NULL)
			return false;
		else
			return DeleteNode(tree, tmp);
	}
}

bool DeleteNode(BSTree* tree, BSTNode* node){
	if(node == NULL)
		return false;
	else{
		if(node->left == NULL and node->right == NULL){ 				// node has no children
			if(node->parent == NULL){
				delete node;
				tree->root = NULL;
			}
			else if(node == node->parent->left){
				node->parent->left = NULL;
				delete node;
			}
			else{
				node->parent->right = NULL;
				delete node;
			}
			return true;
		}
		else if(node->left == NULL or node->right == NULL){ 			// if one of children is null
			if(node->left == NULL){
				if(node->parent == NULL){
					tree->root = node->right;
					delete node;
					tree->root->parent = NULL;
				}
				else{
					if(node == node->parent->left){
						node->parent->left = node->right;
						node->right->parent = node->parent;
						delete node;
					}
					else{
						node->parent->right = node->right;
						node->right->parent = node->parent;
						delete node;
					}
				}
			}
			else{
				if(node->parent == NULL){
					tree->root = node->left;
					delete node;
					tree->root->parent = NULL;
				}
				else{
					if(node == node->parent->left){
						node->parent->left = node->left;
						node->left->parent = node->parent;
						delete node;
					}
					else{
						node->parent->right = node->left;
						node->left->parent = node->parent;
						delete node;
					}
				}
			}
			return true;
		}
		else{ 															// if neither of children is null
			BSTNode* tmp = FindMin(node->right);
			node->key = tmp->key;
			node->value = tmp->value;
			return DeleteNode(tree, tmp);
		}
		}
}


// 11.PrintTreePreOrder()
// a) in whole tree
void PrintTreePreOrder(BSTree* tree){
	PrintTreePreOrder(tree->root);
}

// b) subtree, recursively
void PrintTreePreOrder(BSTNode* node){
	if(node == NULL)
		return;
	cout << "Key: " << node->key << "		Value: " << node->value << endl;
	PrintTreePreOrder(node->left);
	PrintTreePreOrder(node->right);
}

// 12.PrintTreeInOrder()
// a) in whole tree
void PrintTreeInOrder(BSTree* tree){
	PrintTreeInOrder(tree->root);
}

// b) subtree, recursively
void PrintTreeInOrder(BSTNode* node){
	if(node == NULL)
		return;
	PrintTreeInOrder(node->left);
	cout << "Key: " << node->key << "		Value: " << node->value << endl;
	PrintTreeInOrder(node->right);
}

// 13.PrintTreePostOrder()
// a) in whole tree
void PrintTreePostOrder(BSTree* tree){
	PrintTreePostOrder(tree->root);
}

// b) subtree, recursively
void PrintTreePostOrder(BSTNode* node){
	if(node == NULL)
		return;
	PrintTreePostOrder(node->left);
	PrintTreePostOrder(node->right);
	cout << "Key: " << node->key << "		Value: " << node->value << endl;
}

// 14.FindKthInOrder()
// a) in whole tree
bool FindKthInOrder(BSTree* tree, int k){
	int a = 0;
	return FindKthInOrder(tree->root, k, a);
}

// b) subtree, recursively
bool FindKthInOrder(BSTNode* node, int k, int &number){
	if(node == NULL or number > k)
		return false;

	bool t1 = FindKthInOrder(node->left, k, number);
	number++;
	if(k == number){
		cout << "Key: " << node->key << "		Value: " << node->value << endl;
		return true;
	}
	bool t2 = FindKthInOrder(node->right, k, number);
	return t1 or t2;
}

