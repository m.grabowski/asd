// bst.h
#ifndef BST_H
#define BST_H

struct BSTNode;
struct BSTree;

struct BSTNode{
	BSTNode *left, *right, *parent;
	int key, value;

	BSTNode();
	BSTNode(int pKey);
	BSTNode(int pKey, int pValue);
	~BSTNode();
};

struct BSTree{
	BSTNode *root;

	BSTree();
	~BSTree();
};

// Deleting tree
void DeleteTree(BSTree* tree);
void DeleteTree(BSTNode* node);

// Inserting element into tree
void Insert(BSTree* tree, int pKey, int pValue);
void Insert(BSTree* tree, BSTNode* node);

// Find element in tree
BSTNode* FindRecursively(BSTree* tree, int pKey);
BSTNode* FindRecursively(BSTNode* node, int pKey);
BSTNode* Find(BSTree* tree, int pKey);
BSTNode* Find(BSTNode* node, int pKey);

// Find element with min/max key
BSTNode* FindMin(BSTree* tree);
BSTNode* FindMin(BSTNode* node);
BSTNode* FindMax(BSTree* tree);
BSTNode* FindMax(BSTNode* node);

// Find Successor/Predecessor of element
BSTNode* Successor(BSTNode* node);
BSTNode* Predecessor(BSTNode* node);

// DeleteNode
bool DeleteNode(BSTree* tree, int key);
bool DeleteNode(BSTree* tree, BSTNode* node);

// Find kth element in order in tree
bool FindKthInOrder(BSTree* tree, int k);
bool FindKthInOrder(BSTNode* node, int k, int &number);

// Print tree
void PrintTreePreOrder(BSTree* tree);
void PrintTreePreOrder(BSTNode* node);
void PrintTreeInOrder(BSTree* tree);
void PrintTreeInOrder(BSTNode* node);
void PrintTreePostOrder(BSTree* tree);
void PrintTreePostOrder(BSTNode* node);

#endif
