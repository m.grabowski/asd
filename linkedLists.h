// linkedLists.h
#ifndef LINKEDLISTS_H
#define LINKEDLISTS_H

struct Node;

struct Node{
	Node* next;
	int key, value;

	Node();
	Node(int pKey);
	Node(int pKey, int pValue);
	Node(int pKey, int pValue, Node* pNext);
	~Node();
};

// General operations on lists
Node* AddElement(Node* list, int pKey);
Node* AddElement(Node* list, int pKey, int pValue);
Node* AddElement(Node* list, Node* node);
void AddElementRef(Node* &list, Node* node);
void PrintList(Node* list);
void DeleteList(Node* list);

// Finding and deleting elements
Node* FindElement(Node* list, int pKey);
void DeleteNode(Node* list, int pKey);
void DeleteNode(Node* &list, Node* node);

// SelectionSortList()
Node* FindMinList(Node* list);
Node* SelectionSortList(Node* list);

// InsertionSortList()
void InsertElementInOrder(Node* &list, int pKey, int pValue);
void InsertElementInOrder(Node* &list, Node* node);
Node* InsertionSortList(Node* list);

// MergeSortList
Node* GetMiddleElement(Node* list);
Node* MergeLists(Node* list1, Node* list2);
Node* MergeSortList(Node* list);

// QuickSortList
Node* GetLastElement(Node* list);
Node* PartitionList(Node* &list);
Node* QuickSortList(Node* list);

#endif