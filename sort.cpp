#include <iostream>
#include <cstddef>
#include <cstdlib>
#include <ctime>
#include <string>
#include "sort.h"
using namespace std;

/*
   Sorting Algorithms
1. AllocateArray()
2. PrintArray()
3. FillArray()
4. FillArrayRandomly() - fill array with random numbers from 0 to upperLimit
5. DeleteArray()
6. MergeSort()
   a) for launching sorting (array, size)
   b) for recursive merge sort
7. Merge() - used in 6a)
8. QuickSort()
   a) for launching sorting (array,size)
   b) for recursive quick sort
9. Partition() - for quick sort
10. HeapSort procedures:
    a) Left()
    b) Right()
    c) Parent()
    d) HeapifyMax()
    e) BuildHeapMax()
11. HeapSort()
12. CountingSort()
	a) when range is not known
	b) when range is known
13. RadixSortWords() - sort words using counting sort
14. BubbleSort()
15. InsertionSort()
16. GetMinIndex()
17. SelectionSort()
*/

// 1.AllocateArray()
int* AllocateArray(int size){
	if(size <= 0){
		cout << "Cannot allocate array" << endl;
		return NULL;
	}
	else{
		int* array = new int[size];
		return array; 
	}
}

// 2.PrintArray()
void PrintArray(int* array, int size){
	if(array == NULL or size <= 0){
		cout << "Cannot print array elements" << endl;
		return;
	}
	else{
		for(int i = 0; i < size; i++)
			cout << array[i] << ' ';
		cout << endl;
	}
}

// 3.FillArray()
void FillArray(int* array, int size, int number){
	if(array == NULL or size <= 0){
		cout << "Cannot fill array with numbers" << endl;
		return;
	}
	else{
		for(int i = 0; i < size; i++ )
			array[i] = number;
	}
}

// 4.FillArrayRandomly() - fill array with random numbers from 0 to upperLimit
void FillArrayRandomly(int* array, int size){
	if(array == NULL or size <= 0){
		cout << "Cannot fill array with random numbers" << endl;
		return;
	}
	else{
		int upperLimit=11;
		srand(time(NULL));
		for(int i = 0; i < size; i++)
			array[i] = rand() % upperLimit;
	}
}

// 5.DeleteArray()
void DeleteArray(int* array){
	if(array == NULL){
		cout << "Cannot delete array" << endl;
		return;
	}
	else delete[] array;
}

// 6.MergeSort() 
// a) for sorting whole array
void MergeSort(int* array, int size){
	if(array == NULL or size <=0){
		cout << "Cannot sort this array using Mergesort" << endl;
		return;
	}
	else{
		MergeSort(array, 0, size-1);
	}
}

// b) for recursive sorting part of array
void MergeSort(int* array, int start, int end){
	if(start < end){
		int mid = (start + end) / 2;
		MergeSort(array, start, mid);
		MergeSort(array, mid + 1, end);
		Merge(array, start, mid, end);
	}
}

// 7.Merge() - for MergeSort() - end must be included in all inequalities
void Merge(int* array, int start, int mid, int end){
	int* tmpArray = AllocateArray(end - start + 1);
	int left = start;
	int right = mid+1;
	int tmp = 0;
	while(left <= mid and right <= end){
		if(array[left] <= array[right]){
			tmpArray[tmp] = array[left];
			tmp++;
			left++;
		}
		else{
			tmpArray[tmp] = array[right];
			tmp++;
			right++;
		}
	}
	while(left <= mid){
		tmpArray[tmp] = array[left];
		tmp++;
		left++;
	}
	while(right <= end){
		tmpArray[tmp] = array[right];
		tmp++;
		right++;
	}
	for(int i = start; i <= end; i++)
		array[i] = tmpArray[i-start];
	DeleteArray(tmpArray);
}

// 8.QuickSort()
// a) for sorting whole array
void QuickSort(int* array, int size){
	if(array == NULL or size <= 0){
		cout << "Cannot sort this array using QuickSort" << endl;
		return;
	}
	else{
		QuickSort(array, 0, size - 1);
	}
}

// b) for sorting recursively
void QuickSort(int* array, int start, int end){
	if(start < end){
		int pivot = Partition(array, start, end);
		QuickSort(array, start, pivot-1);
		QuickSort(array, pivot+1, end);
	}
}

// 9.Partition()
int Partition(int* array, int start, int end){
	int pivot = array[end];
	int i = start -1;
	for(int j = start; j < end; j++){
		if(array[j] <= pivot){
			i++;
			swap(array[i], array[j]);
		}
	}
	swap(array[i + 1], array[end]);
	return i + 1;
}

// 10.HeapSort procedures
// a) Left() - return index of left son
int Left(int index){
	return 2 * index + 1;
}

// b) Right() - return index of right son
int Right(int index){
	return 2 * index + 2;
}

// c) Parent() - return index of parent
int Parent(int index){
	return (index - 1) / 2;
}

// d) HeapifyMax() - restore Max Heap property
void HeapifyMax(int* array, int size, int index){
	int left = Left(index);
	int right = Right(index);
	int largest = index;
	if(left < size and array[left] > array[index])
		largest = left;
	if(right < size and array[right] > array[largest])
		largest = right;
	if(largest != index){
		swap(array[index], array[largest]);
		HeapifyMax(array, size, largest);
	}
}

// e) BuildHeapMax() - Build maxHeap based on array
void BuildHeapMax(int* array, int size){
	for(int i = size / 2; i >= 0; i--)
		HeapifyMax(array, size, i);
}

// 11.HeapSort()
void HeapSort(int* array, int size){
	BuildHeapMax(array, size);
	int length = size;
	for(int i = size - 1; i >= 1; i--){
		swap(array[i], array[0]);
		length--;
		HeapifyMax(array, length, 0);
	}	
}

// 12.CountingSort()
// a) when range is not known
void CountingSort(int* array, int size){
	if(array == NULL or size <= 0){
		cout << "Cannot sort this array using CountingSort" << endl;
		return;
	}
	else{
		int maxValue = array[0];
		for(int i = 1; i < size; i++)
			if(array[i] > maxValue)
				maxValue = array[i];
		CountingSort(array, size, maxValue);
	}
}

// b) when range is known
void CountingSort(int* array, int size, int range){
	cout << range << endl;
	range++;
	int* numberArray = AllocateArray(range);
	int* tmpArray = AllocateArray(size);

	for(int i = 0; i < range; i++)		// fill tab with 0's
		numberArray[i] = 0;
	
	for(int i = 0; i < size; i++)
		numberArray[array[i]]++;

	for(int i = 1; i < range; i++)
		numberArray[i] += numberArray[i - 1];

	for(int i = size - 1; i >= 0; i--)
		tmpArray[--numberArray[array[i]]] = array[i];

	for(int i = 0; i < size; i++) 		// copy back
		array[i] = tmpArray[i];

	DeleteArray(numberArray);
	DeleteArray(tmpArray);
}
		
// 13.RadixSortWords() 											// for words consisting of lowercase letters
void RadixSortWords(string* array, int size){ 					// you can compare strings in C++, this is proof that RadixSort performed on words can sort them lexicographically
	if(array == NULL or size <= 0){
		cout << "Cannot sort this array using RadixSort" << endl;
		return;
	}
	else{
		int maxLength=0;
		for(int i = 0; i < size; i++){
			if(array[i].length() > maxLength){
				maxLength = array[i].length();
			}
		}

		int* characterCounterArray = AllocateArray(27); 		// for 26 letters and a ' '
		string* tmpArray = new string[size];
		

		for(int i = maxLength - 1; i >= 0; i--){ 				// perform counting sort (maxLength) times
			for(int j = 0; j < 27; j++)
				characterCounterArray[j] = 0;

			for(int j = 0; j < size; j++){
				if(array[j].length() - 1 < i)
					characterCounterArray[0]++;
				else if((int) (array[j][i]) >= 97 and (int) (array[j][i]) <= 122)
					characterCounterArray[(int) (array[j][i]) - 96]++;
			}

			for(int j = 1; j < 27; j++)
				characterCounterArray[j] += characterCounterArray[j-1];

			for(int j = size - 1; j >= 0; j--){
				if(array[j].length() - 1 < i)
					tmpArray[--characterCounterArray[0]] = array[j];
				else
					tmpArray[--characterCounterArray[(int) (array[j][i]) - 96]] = array[j];
			}

			for(int j = 0; j < size; j++) 						// copy back
				array[j] = tmpArray[j];
		}

		DeleteArray(characterCounterArray);
		delete[] tmpArray;
	}
}

// 14.BubbleSort()
void BubbleSort(int* array, int size){
	if(array == NULL or size <= 0){
		cout << "Cannot sort this array using BubbleSort" << endl;
		return; 
	}
	else{
		bool change;
		for(int i = 0; i < size; i++){
			change = false;
			for(int j = 0; j < size - 1; j++)
				if(array[j + 1] < array[j]){
					swap(array[j], array[j + 1]);		
					change = true;	
				}
			if(!change)
				break;
		}
	}
}

// 15.InsertionSort()
void InsertionSort(int* array, int size){
	if(array == NULL or size <= 0){
		cout << "Cannot sort this array using InsertionSort" << endl;
		return;
	}
	else{
		int j, tmp;
		for(int i = 1; i < size; i++){
			tmp = array[i];
			for(j = i - 1; j >= 0 and array[j] > tmp; j--)
				array[j+1] = array[j];
			array[j+1] = tmp;
		}
	}
}

// 16.GetMinIndex()
int GetMinIndex(int* array, int size, int toIndex){
	if(array == NULL or size <= 0 or toIndex < 0 or toIndex >= size){
		cout << "Cannot get index of smallest element before toIndex" << endl;
		return -1;
	}
	else{
		int index = toIndex;
		int tmp = array[toIndex];
		for(int i = toIndex + 1; i < size; i++)
			if(array[i] < tmp){
				index = i;
				tmp = array[i];
			}
		return index;
	}
}


// 17.SelectionSort()
void SelectionSort(int* array, int size){
	for(int i = 0; i < size; i++){
		int tmp = GetMinIndex(array, size, i);
		if(tmp != i)
			swap(array[i], array[tmp]);
	}
}

